package com.example.calculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import kotlinx.android.synthetic.main.activity_main.*

class CalculatorActivity : AppCompatActivity(), View.OnClickListener {
    private var firstVariable : Double = 0.0
    private var secondVariable : Double = 0.0
    private var operation = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        button0.setOnClickListener(this)
        button1.setOnClickListener(this)
        button2.setOnClickListener(this)
        button3.setOnClickListener(this)
        button4.setOnClickListener(this)
        button5.setOnClickListener(this)
        button6.setOnClickListener(this)
        button7.setOnClickListener(this)
        button8.setOnClickListener(this)
        button9.setOnClickListener(this)

        buttonPoint.setOnClickListener {
            if(resultTextView.text.isNotEmpty() && "." !in resultTextView.text.toString()) {
                resultTextView.text = resultTextView.text.toString() + "."
            }
        }
    }
    fun multiplication(view: View) {
        val value = resultTextView.text.toString()
        if(value.isNotEmpty()){
            firstVariable = value.toDouble()
            operation = "x"
            resultTextView.text = ""
        }
    }

    fun substraction(view: View){
        val value = resultTextView.text.toString()
        if(value.isNotEmpty()){
            firstVariable = value.toDouble()
            operation = "-"
            resultTextView.text = ""
        }
    }

    fun addition(view: View){
        val value = resultTextView.text.toString()
        if(value.isNotEmpty()){
            firstVariable = value.toDouble()
            operation = "+"
            resultTextView.text = ""
        }
    }

    fun equal(view: View){
        val value = resultTextView.text.toString()
        if(value.isNotEmpty() && operation.isNotEmpty()){
        secondVariable = value.toDouble()
        var result:Double = 0.0
        if(operation == ":"){
            result = (firstVariable/secondVariable).toDouble()
        }
            if(operation == "X") {
                result = firstVariable * secondVariable.toDouble()
            }
            if(operation == "+"){
                result = firstVariable + secondVariable.toDouble()
            }
            if(operation == "-"){
                result = firstVariable - secondVariable.toDouble()
            }
        resultTextView.text = result.toString()

            operation = ""
            firstVariable = 0.0
            secondVariable = 0.0
            }
    }

    fun divide(view: View){
        val value = resultTextView.text.toString()
        if(value.isNotEmpty()) {
            firstVariable = value.toDouble()
            operation = ":"
            resultTextView.text = ""
        }
    }

    fun delete(view: View){
        val value = resultTextView.text.toString()
        if(value.isNotEmpty())
        resultTextView.text = value.substring(startIndex = 0, value.length - 1)
    }

    override fun onClick(v: View?) {
        val button : Button = v as Button
        resultTextView.text = resultTextView.text.toString() + button.text.toString()
    }
}

